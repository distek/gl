# golaunch

Init system to ease the pain that is launchctl

## Installation

```
git clone git@codeberg.org:distek/golaunch.git gl && cd gl
go build . && cp gl /usr/local/bin/gl
```

## Usage

Use this launchctl plist as the final abstraction away from launchctl (replace USERNAME with your username under the PATH key):
`~/Library/LaunchAgents/golaunch.plist`

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
    <key>EnvironmentVariables</key>
    <dict>
    <key>PATH</key>
    <string>/usr/local/bin:/Users/USERNAME/.local/bin:/usr/bin:/bin:/usr/sbin:/sbin:</string>
    </dict>
    <key>Label</key>
    <string>com.golaunch</string>
    <key>ProgramArguments</key>
    <array>
    <string>/usr/local/bin/gl</string>
    <string>-d</string>
    </array>
    <key>RunAtLoad</key>
    <true/>
    <key>KeepAlive</key>
    <true/>
    <key>StandardOutPath</key>
    <string>/tmp/golaunch.stdout</string>
    <key>StandardErrorPath</key>
    <string>/tmp/golaunch.stderr</string>
    <key>InitGroups</key>
    <true/>
</dict>
</plist>
```

Load the launchctl service with:

```
launchctl load -w ~/Library/LaunchAgents/golaunch.plist
```

Create the service directories:

```
mkdir -p ~/.config/golaunch/{enabled,staging}
```

Add a service (example is `~/.config/golaunch/staging/yabai.yaml`):

```
name: yabai
description: yabai service

# pre-exec: echo "some pre command"
exec: yabai
# post-exec: echo "some post command"

restart-attempts: 10
restart-delay: 5s
```

Enable a service (continuing with the yabai example):

```
gl enable yabai
```

and start it:

```
gl start yabai
```

or do both at the same time with:

```
gl enable yabai --now
```

stop a service:

```
gl stop yabai
```

restart a service:

```
gl restart yabai
```

disable a service:

```
gl disable yabai
```

## Notes

Completions can be generated with:

```
golaunch completion <your shell> > /path/to/your/completions/_golaunch
```

For example, ZSH:

```
golaunch completion zsh > /usr/local/share/zsh/site-functions/_golaunch
```
