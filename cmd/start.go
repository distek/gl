package cmd

import (
	"fmt"
	"strings"

	"github.com/spf13/cobra"
)

var startCmd = &cobra.Command{
	Use:   "start <service>[,<service>]",
	Short: "start service(s)",
	ValidArgsFunction: func(
		cmd *cobra.Command,
		args []string,
		toComplete string,
	) ([]string, cobra.ShellCompDirective) {
		var ret []string

		ret = append(ret, strings.Split(client(compGetNotRunning, nil), "\n")[:]...)

		return ret, cobra.ShellCompDirectiveNoFileComp
	},
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Print(client("start", args))
	},
}

func init() {
	rootCmd.AddCommand(startCmd)
}
