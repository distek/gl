package cmd

const (
	compGetAllNames    = "__getAllNames"
	compGetAllDisabled = "__getAllDisabled"
	compGetAllEnabled  = "__getAllEnabled"
	compGetStarted     = "__getStarted"
	compGetNotRunning  = "__getNotRunning"
)
