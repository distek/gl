package cmd

import (
	"bufio"
	"io"
	"log"
	"net"
	"strings"
	"time"
)

func compileArgs(cmd string, in []string) string {
	var bld strings.Builder

	bld.WriteString(cmd + " ")

	for i, v := range in {
		bld.WriteString(v)

		if i+1 != len(in) {
			bld.WriteRune(' ')
		}
	}

	return bld.String()
}

func client(cmd string, args []string) string {
	conn, err := net.Dial("unix", sockAddr)

	if err != nil {
		log.Fatal(err)
	}

	defer conn.Close()

	conn.SetWriteDeadline(time.Now().Add(time.Second * 2))

	_, err = conn.Write([]byte(compileArgs(cmd, args) + "\n"))
	if err != nil {
		log.Println(err)
	}

	reader := bufio.NewReader(conn)
	var bld strings.Builder
	for {
		b, err := reader.ReadByte()
		if err != nil {
			if err == io.EOF {
				break
			}
			log.Fatal(err)
		}

		bld.WriteByte(b)
	}

	return bld.String()
}
