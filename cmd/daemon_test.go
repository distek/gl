package cmd

import (
	"reflect"
	"testing"
)

func Test_splitQuoteAware(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		name string
		args args
		want []string
	}{
		{
			name: "double",
			args: args{
				s: "This is a \"quoted string\" with whitespace",
			},
			want: []string{"This", "is", "a", "\"quoted string\"", "with", "whitespace"},
		},
		{
			name: "single",
			args: args{
				s: "This is a 'quoted string' with whitespace",
			},
			want: []string{"This", "is", "a", "'quoted string'", "with", "whitespace"},
		},
		{
			name: "backtick",
			args: args{
				s: "This is a `quoted string` with whitespace",
			},
			want: []string{"This", "is", "a", "`quoted string`", "with", "whitespace"},
		},
		{
			name: "new-line test",
			args: args{
				s: "This is a \n`quoted\nstring` \nwith whitespace",
			},
			want: []string{"This", "is", "a", "`quoted string`", "with", "whitespace"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := splitQuoteAware(tt.args.s); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("splitQuoteAware() = %v, want %v", got, tt.want)
			}
		})
	}
}
