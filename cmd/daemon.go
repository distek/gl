package cmd

import (
	"bufio"
	"fmt"
	"log"
	"net"
	"os"
	"runtime"
	"strings"

	"github.com/spf13/cobra"
)

var (
	sockAddr  = "/tmp/golaunch.socket"
	started   []string
	services  = make(map[string]*service, 1)
	systemEnv []string
)

func connPrint(msgType, msg string) []byte {
	switch msgType {
	case "usage":
		return []byte(fmt.Sprintln("This is where usage would be normally"))
	case "error":
		return []byte(fmt.Sprintf("ERROR: %s\n", msg))
	case "info":
		return []byte(fmt.Sprintf("INFO: %s\n", msg))
	case "not-found":
		return []byte(fmt.Sprintf("Service not found: %s\n", msg))
	case "status": // msg should be service name
		svc := findService(msg)
		if svc == nil {
			return []byte(fmt.Sprintln("Unable to find service:", msg))
		}
		var status strings.Builder
		status.WriteString(fmt.Sprintf("Name        : %s\n", svc.Name))

		status.WriteString(fmt.Sprintf("Description : %s\n", svc.Description))

		var state string
		switch svc.state {
		case 0:
			state = "Started"
		case 1:
			state = "Stopped"
		case 2:
			state = "Restarting"
		}

		status.WriteString(fmt.Sprintf("Status      : %s\n", state))

		if svc.Loop {
			status.WriteString(fmt.Sprintf("Time until  : %s\n", svc.loopTimeDur.String()))
		}

		status.WriteString("--- Log:\n")

		for _, v := range svc.logs.Lines {
			status.WriteString(v + "\n")
		}

		return []byte(status.String())
	}

	return []byte{'\n'}
}

func daemonStartCall(conn net.Conn, split []string) {
	var notFound []string

	findServices()

	for _, v := range split[1:] {
		svc := findService(v)
		if svc == nil {
			notFound = append(notFound, v)

			continue
		}

		if svc.state == SERVICE_STARTED {
			svc.Logger("Already started...")

			_, err := conn.Write(connPrint("error",
				"service already started: "+svc.Name),
			)
			if err != nil {
				log.Println(err)
			}

			return
		}

		go svc.startService()

		svc.state = SERVICE_STARTED

		svc.Logger("Starting...")

		_, err := conn.Write(connPrint("INFO",
			"service started: "+svc.Name),
		)
		if err != nil {
			log.Println(err)
		}
	}

	for _, v := range notFound {
		_, err := conn.Write(connPrint("not-found", v))
		if err != nil {
			log.Println(err)
		}
	}
}

func daemonStatusCall(conn net.Conn, split []string) {
	if len(split) < 2 {
		_, err := conn.Write(connPrint("error",
			"provide at least one service name",
		))

		if err != nil {
			log.Println(err)
		}

		return
	}

	if len(split) > 2 {
		_, err := conn.Write(connPrint("info",
			"NOTE: status only provides info for one service at a time",
		))

		if err != nil {
			log.Println(err)
		}
	}

	_, err := conn.Write(connPrint("status", split[1]))
	if err != nil {
		log.Println(err)
	}
}

func daemonRestartCall(conn net.Conn, split []string) {
	var notFound []string

	if split[1] == "all" {
		for k, v := range services {
			v.kill <- true
			delete(services, k)
		}

		_, err := conn.Write(connPrint("info", "Restarting all services..."))
		if err != nil {
			log.Println(err)
		}

		findServices()
		go startServices()

		return
	}

	for _, v := range split[1:] {
		svc := findService(v)
		if svc == nil {
			notFound = append(notFound, v)

			continue
		}

		loadService(svc.filePath)

		svc.Logger("Restarting...")
		svc.state = SERVICE_RESTARTING

		_, err := conn.Write(connPrint("info", "Restarting: "+svc.Name))
		if err != nil {
			log.Println(err)
		}

		svc.kill <- true

		go svc.startService()

		svc.state = SERVICE_STARTED

		_, err = conn.Write(connPrint("INFO",
			"service started: "+svc.Name),
		)
		if err != nil {
			log.Println(err)
		}
	}

	for _, v := range notFound {
		_, err := conn.Write(connPrint("not-found", v))
		if err != nil {
			log.Println(err)
		}
	}
}

func daemonStopCall(conn net.Conn, split []string) {
	var notFound []string

	for _, v := range split[1:] {
		svc := findService(v)
		if svc == nil {
			notFound = append(notFound, v)

			continue
		}

		if svc.state == SERVICE_STOPPED {
			svc.Logger("Already stopped...")
			_, err := conn.Write(connPrint("error",
				"service already stopped: "+svc.Name),
			)
			if err != nil {
				log.Println(err)
			}

			return
		}

		svc.Logger("Stopping...")

		_, err := conn.Write(connPrint("info", "Stopping: "+svc.Name))
		if err != nil {
			log.Println(err)
		}

		svc.kill <- true

		svc.state = SERVICE_STOPPED
	}

	for _, v := range notFound {
		_, err := conn.Write(connPrint("not-found", v))
		if err != nil {
			log.Println(err)
		}
	}
}

func daemonDisableCall(conn net.Conn, split []string) {
	findServices()

	now := split[1] == "true"

	for _, v := range split[2:] {
		if _, ok := services[v]; ok {
			if !services[v].isEnabled {
				_, err := conn.Write([]byte(fmt.Sprintf("Already disabled: %s\n", v)))
				if err != nil {
					log.Println(err)
				}

				continue
			}

			services[v].isEnabled = false

			pathSplit := strings.Split(services[v].filePath, "/")

			err := os.Remove(configDir + "enabled/" + pathSplit[len(pathSplit)-1])
			if err != nil {
				_, err = conn.Write([]byte(fmt.Sprintln("Unable to remove link: ", err)))
				if err != nil {
					log.Println(err)

					continue
				}
			}

			_, err = conn.Write([]byte(fmt.Sprintf("Disabled: %s\n",
				services[v].Name,
			)))
			if err != nil {
				log.Println(err)

				continue
			}

			if now {
				services[v].kill <- true
				_, err = conn.Write([]byte(fmt.Sprintf("Stopped: %s\n", v)))
				if err != nil {
					log.Println(err)
				}
			}
		}
	}
}

func daemonEnableCall(conn net.Conn, split []string) {
	findServices()

	now := split[1] == "true"

	for _, v := range split[2:] {
		if _, ok := services[v]; ok {
			if services[v].isEnabled {
				_, err := conn.Write([]byte(fmt.Sprintf("Already enabled: %s\n", v)))
				if err != nil {
					log.Println(err)
				}

				continue
			}
			services[v].isEnabled = true

			pathSplit := strings.Split(services[v].filePath, "/")

			err := os.Symlink(
				services[v].filePath,
				configDir+"enabled/"+pathSplit[len(pathSplit)-1],
			)
			if err != nil {
				_, err = conn.Write([]byte(fmt.Sprintln("Unable to link file: ", err)))
				if err != nil {
					log.Println(err)

					continue
				}
			}

			_, err = conn.Write([]byte(fmt.Sprintf("Enabled: %s -> %s\n",
				services[v].filePath,
				configDir+"enabled/"+pathSplit[len(pathSplit)-1],
			)))
			if err != nil {
				log.Println(err)

				continue
			}

			if now {
				go services[v].startService()

				_, err = conn.Write([]byte(fmt.Sprintf("Started: %s\n", v)))
				if err != nil {
					log.Println(err)
				}
			}
		}
	}
}

func handle(conn net.Conn) {
	defer conn.Close()

	var req strings.Builder

	scanner := bufio.NewScanner(conn)

	scanner.Split(bufio.ScanBytes)

	for scanner.Scan() {
		b := scanner.Bytes()
		if b[0] == '\n' {
			break
		}

		req.WriteByte(b[0])
	}

	split := splitQuoteAware(req.String())
	if len(split) <= 0 {
		_, err := conn.Write([]byte(connPrint("usage", "")))
		if err != nil {
			log.Println(err)
		}

		return
	}

	switch split[0] {
	case "status":
		daemonStatusCall(conn, split)

	case "restart":
		daemonRestartCall(conn, split)

	case "stop":
		daemonStopCall(conn, split)

	case "start":
		daemonStartCall(conn, split)

	case "enable":
		daemonEnableCall(conn, split)

	case "disable":
		daemonDisableCall(conn, split)

	// completion bits
	case compGetAllNames:
		var bld strings.Builder

		for _, v := range services {
			bld.WriteString(v.Name + "\n")
		}

		_, err := conn.Write([]byte(bld.String()))
		if err != nil {
			log.Println(err)
		}
	case compGetAllEnabled:
		var bld strings.Builder

		for _, v := range services {
			if v.isEnabled {
				bld.WriteString(v.Name + "\n")
			}
		}

		_, err := conn.Write([]byte(bld.String()))
		if err != nil {
			log.Println(err)
		}
	case compGetAllDisabled:
		var bld strings.Builder

		for _, v := range services {
			if !v.isEnabled {
				bld.WriteString(v.Name + "\n")
			}
		}

		_, err := conn.Write([]byte(bld.String()))
		if err != nil {
			log.Println(err)
		}

	case compGetStarted:
		var bld strings.Builder

		for _, v := range services {
			if v.state == SERVICE_STARTED || v.state == SERVICE_RESTARTING {
				bld.WriteString(v.Name + "\n")
			}
		}

		_, err := conn.Write([]byte(bld.String()))
		if err != nil {
			log.Println(err)
		}

	case compGetNotRunning:
		var bld strings.Builder

		for _, v := range services {
			if v.state != SERVICE_STARTED && v.state != SERVICE_RESTARTING {
				bld.WriteString(v.Name + "\n")
			}
		}

		_, err := conn.Write([]byte(bld.String()))
		if err != nil {
			log.Println(err)
		}
	default:
		_, err := conn.Write([]byte(fmt.Sprintf(`
unknown command: %s
whole line     : %s
		`,
			split[0],
			req.String(),
		)))

		if err != nil {
			log.Println(err)
		}
	}
}

var daemonCmd = &cobra.Command{
	Use:   "daemon",
	Short: "Daemon portion",
	Run: func(cmd *cobra.Command, args []string) {
		if err := os.RemoveAll(sockAddr); err != nil {
			log.Fatal(err)
		}

		listener, err := net.Listen("unix", sockAddr)
		if err != nil {
			log.Fatal(err)
		}

		initGolaunchFiles()
		findServices()
		go startServices()

		switch runtime.GOOS {
		case "linux":
			getSystemdEnvironment()
		case "darwin":
			// TODO: Figure this out
			// might just have to source a rc file cause macos
		}

		for {
			conn, err := listener.Accept()
			if err != nil {
				log.Println(err)
				continue
			}

			go handle(conn)
		}
	},
}

func init() {
	rootCmd.AddCommand(daemonCmd)
}
