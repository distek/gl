package cmd

import (
	"crypto/sha256"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"syscall"
	"time"

	"gopkg.in/yaml.v2"
)

type serviceState int

const (
	SERVICE_STARTED serviceState = iota
	SERVICE_STOPPED
	SERVICE_RESTARTING
)

type logs struct {
	Name    string
	Lines   []string
	LogFile *os.File
}

type service struct {
	Name            string            `yaml:"name"`
	Description     string            `yaml:"description"`
	Env             map[string]string `yaml:"env"`
	RestartAttempts int               `yaml:"restart-attempts"`
	RestartDelay    string            `yaml:"restart-delay"`
	PreExec         string            `yaml:"pre-exec"`
	PostExec        string            `yaml:"post-exec"`
	Exec            string            `yaml:"exec"`
	Loop            bool              `yaml:"loop"`
	LoopTime        string            `yaml:"loop-time"`
	After           []string          `yaml:"after"`
	isEnabled       bool
	filePath        string
	fileHash        []byte
	logs            logs
	state           serviceState
	envSlice        []string
	loopTimeDur     time.Duration
	restartDelayDur time.Duration
	pid             int
	kill            chan bool
}

func startServices() {
	for _, v := range services {
		if !v.isEnabled {
			continue
		}

		go v.startService()
	}
}

func loadService(p string) string {
	var thisService service

	f, err := os.ReadFile(p)
	if err != nil {
		log.Println(err)
		return ""
	}

	h := sha256.New()

	_, err = h.Write(f)
	if err != nil {
		log.Println(err)
		return ""
	}

	sameHash := false

	hash := h.Sum(nil)

	err = yaml.Unmarshal(f, &thisService)
	if err != nil {
		log.Println(err)
		return ""
	}

	for _, v := range services {
		if v.Name == thisService.Name {
			return v.Name
		}
	}

	thisService.filePath = p

	for k, v := range services {
		if thisService.Name == k {
			sameHash = compareHash(hash, v.fileHash)
			if sameHash {
				log.Println("Service already loaded:", k)

				return ""
			}
		}
	}

	thisService.fileHash = hash

	thisService.processEnvs()

	if thisService.LoopTime != "" {
		thisService.loopTimeDur, err = time.ParseDuration(thisService.LoopTime)
		if err != nil {
			log.Println(err)
			return ""
		}
	}

	if thisService.RestartDelay != "" {
		thisService.restartDelayDur, err = time.ParseDuration(thisService.RestartDelay)
		if err != nil {
			log.Println(err)
			return ""
		}
	}

	thisService.kill = make(chan bool, 1)

	if !sameHash {
		log.Println("Loaded service:", thisService.Name)
	} else {
		log.Println("Found service:", thisService.Name)
	}

	thisService.state = -1

	logFile := cacheDir + "/" + thisService.Name + ".log"
	if exists(logFile) {
		err = os.Rename(logFile, logFile+".old")
		if err != nil {
			log.Println(err)
		}
	}

	thisService.logs.LogFile, err = os.Create(logFile)
	if err != nil {
		log.Println("Unable to open log file for service: ", thisService.Name)
	}

	services[thisService.Name] = &thisService

	return thisService.Name
}

func walkerTexasRanger(p string) []string {
	var files []string
	err := filepath.Walk(p,
		func(path string, info os.FileInfo, err error) error {
			if info.IsDir() {
				return nil
			}
			files = append(files, path)

			return nil
		},
	)

	if err != nil {
		log.Println(err)
	}

	return files
}

// log.Println wrapper prepending service's name
func (s *service) Logger(msg ...interface{}) {
	line := fmt.Sprintf("%s %s: %s", prettyTime(), s.Name, msg)

	// keep 50 entries in the in-memory log
	if len(s.logs.Lines) > 25 {
		s.logs.Lines = s.logs.Lines[1:]
	}

	s.logs.Lines = append(s.logs.Lines, fmt.Sprint(line))

	_, err := s.logs.LogFile.WriteString(line + "\n")
	if err != nil {
		log.Println(err)
	}

	fmt.Println(line)
}

func (s *service) processEnvs() {
	var envSlice []string

	for k, v := range s.Env {
		envSlice = append(envSlice, fmt.Sprintf("%s=%s", k, v))
	}

	s.envSlice = envSlice
}

func (s *service) killProc() {
	p, err := os.FindProcess(s.pid)
	if err != nil {
		return
	}

	// Term
	err = p.Signal(syscall.Signal(15))
	if err != nil {
		// OR MURDER?!
		err = p.Signal(syscall.Signal(9))
		if err != nil {
			s.Logger(err)
		}
	}
}

func (s *service) startService() {
	if len(s.After) > 0 {
		s.Logger("Checking \"after\" services...")
	checkStarted:
		var afterCount []string

		for _, v := range services {
			if !ifInSlice(started, v.Name) && ifInSlice(s.After, v.Name) {
				afterCount = append(afterCount, v.Name)
			}
		}

		if len(afterCount) > 0 {
			s.Logger("Cannot start, waiting on services:")

			for _, v := range afterCount {
				s.Logger(v)
			}

			time.Sleep(time.Second * 1)

			goto checkStarted
		}
	}

	go func() {
		for {
			select {
			case <-s.kill:
				s.killProc()
			default:
				time.Sleep(time.Second * 1)
			}
		}
	}()

	restartCount := 0

	// Pre-exec
	if s.PreExec != "" {
		curPID, curCmd := runCommand(s, s.PreExec)

	preExecRestart:
		for isRunning(curPID) {
			time.Sleep(time.Second * 1)
		}

		if s.state == SERVICE_STOPPED || s.state == SERVICE_RESTARTING {
			return
		}

		if curCmd.ProcessState.ExitCode() > 0 {
			s.Logger("Process exited with exit code:",
				curCmd.ProcessState.ExitCode(),
			)

			if s.RestartAttempts != restartCount {
				restartCount++

				time.Sleep(s.restartDelayDur)

				goto preExecRestart
			} else {
				s.Logger("Failed to run service",
					"Restart threshold met in pre-exec phase",
				)

				return
			}
		}

		s.pid = -1
	}

	s.state = SERVICE_STARTED

	// Exec
	for {
		curPID, curCmd := runCommand(s, s.Exec)

		started = append(started, s.Name)
	execRestart:
		for isRunning(curPID) {
			time.Sleep(time.Second * 1)
		}

		if s.state == SERVICE_STOPPED || s.state == SERVICE_RESTARTING {
			return
		}

		if curCmd.ProcessState.ExitCode() > 0 {
			s.Logger("Process exited with exit code:",
				curCmd.ProcessState.ExitCode(),
			)
			if s.RestartAttempts != restartCount {
				restartCount++

				time.Sleep(s.restartDelayDur)

				goto execRestart
			} else {
				s.Logger("Failed to start service",
					"Restart threshold met in exec phase",
				)
				return
			}
		}

		s.pid = -1

		if s.Loop {
			s.Logger("Sleeping for", s.loopTimeDur.String())
			time.Sleep(s.loopTimeDur)
		} else {
			break
		}
	}
	// Post-exec
	if s.PostExec != "" {
		curPID, curCmd := runCommand(s, s.PostExec)

	postExecRestart:
		for isRunning(curPID) {
			time.Sleep(time.Second * 1)
		}

		if s.state == SERVICE_STOPPED || s.state == SERVICE_RESTARTING {
			return
		}

		if curCmd.ProcessState.ExitCode() > 0 {
			s.Logger("Process exited with exit code:",
				curCmd.ProcessState.ExitCode(),
			)
			if s.RestartAttempts != restartCount {
				restartCount++

				time.Sleep(s.restartDelayDur)
				goto postExecRestart
			} else {
				s.Logger("Failed to finish service",
					"Restart threshold met in post-exec phase",
				)
				return
			}
		}

		s.pid = -1
	}
}
