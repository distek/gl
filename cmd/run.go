package cmd

import (
	"bufio"
	"log"
	"os"
	"os/exec"
	"syscall"
)

func runCommand(s *service, cmdString string) (int, *exec.Cmd) {
	cmd := exec.Command("sh", "-c", cmdString)

	cmd.Env = append(systemEnv, s.envSlice[:]...)

	stderr, err := cmd.StderrPipe()
	if err != nil {
		s.Logger("could not pipe stderr")
	} else {
		go func() {
			scn := bufio.NewScanner(stderr)

			for scn.Scan() {
				s.Logger("stderr:", scn.Text())
			}
		}()
	}

	stdout, err := cmd.StdoutPipe()
	if err != nil {
		s.Logger("could not pipe stdout")
	} else {
		go func() {
			scn := bufio.NewScanner(stdout)

			for scn.Scan() {
				s.Logger("stdout:", scn.Text())
			}
		}()
	}

	err = cmd.Start()
	if err != nil {
		log.Println(err)
		return -1, nil
	}

	s.pid = cmd.Process.Pid

	go func() {
		err = cmd.Wait()
		if err != nil {
			s.Logger(err)
		}
	}()

	return s.pid, cmd
}

func isRunning(pid int) bool {
	p, err := os.FindProcess(pid)
	if err != nil {
		return false
	}

	err = p.Signal(syscall.Signal(0))

	return err == nil
}
