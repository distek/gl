package cmd

import (
	"fmt"
	"strings"

	"github.com/spf13/cobra"
)

var stopCmd = &cobra.Command{
	Use:   "stop <service>[,<service>]",
	Short: "stop service(s)",
	ValidArgsFunction: func(
		cmd *cobra.Command,
		args []string,
		toComplete string,
	) ([]string, cobra.ShellCompDirective) {
		var ret []string

		ret = append(ret, strings.Split(client(compGetStarted, nil), "\n")[:]...)

		return ret, cobra.ShellCompDirectiveNoFileComp
	},
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Print(client("stop", args))
	},
}

func init() {
	rootCmd.AddCommand(stopCmd)
}
