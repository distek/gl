package cmd

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"strings"
	"time"
)

var (
	home = os.Getenv("HOME")

	configDir = home + "/.config/golaunch/"
	cacheDir  = home + "/.cache/golaunch/"
)

func getSystemdEnvironment() {
	cmd, err := exec.Command("sh", "-c",
		"systemctl show-environment --user",
	).Output()

	if err != nil {
		log.Println(err)
	}

	systemEnv = strings.Split(string(cmd), "\n")
}

func prettyTime() string {
	t := time.Now()
	return fmt.Sprintf("%d/%02d/%02d %02d:%02d:%02d",
		t.Year(), t.Month(), t.Day(), t.Hour(), t.Minute(), t.Second())
}

func exists(p string) bool {
	_, err := os.Stat(p)
	return err == nil
}

func initGolaunchFiles() {
	if !exists(configDir + "enabled/") {
		err := os.MkdirAll(configDir+"enabled/", 0750)
		if err != nil {
			log.Fatal(err)
			return
		}
	}

	if !exists(configDir + "staging/") {
		err := os.MkdirAll(configDir+"staging/", 0750)
		if err != nil {
			log.Fatal(err)
			return
		}
	}

	if !exists(cacheDir) {
		err := os.MkdirAll(cacheDir, 0750)
		if err != nil {
			log.Fatal(err)
			return
		}
	}
}

func findServices() {
	for _, v := range walkerTexasRanger(configDir + "staging/") {
		_ = loadService(v)
	}

	for _, v := range walkerTexasRanger(configDir + "enabled/") {
		thisSvc := loadService(v)
		if thisSvc != "" {
			services[thisSvc].isEnabled = true
		}
	}
}

func compareHash(h1, h2 []byte) bool {
	if len(h1) != len(h2) {
		return false
	}

	for i := 0; i < len(h1); i++ {
		if h1[i] != h2[i] {
			return false
		}
	}

	return true
}

func findService(name string) *service {
	for k, v := range services {
		if k == name {
			return v
		}
	}

	return nil
}

func ifInSlice(slice []string, item string) bool {
	for _, v := range slice {
		if v == item {
			return true
		}
	}

	return false
}

func splitQuoteAware(s string) []string {
	var ret []string

	inQuote := false
	prevCharEsc := false

	var lastQuote rune

	var bld strings.Builder
	for i, v := range s {
		if v == '"' || v == '\'' || v == '`' {
			if !prevCharEsc {
				if inQuote {
					if lastQuote == v {
						inQuote = false
					}
					goto breakQuoteIf
				}
			}
			inQuote = true
			lastQuote = v
		}

	breakQuoteIf:
		if v == ' ' {
			if !inQuote {
				ret = append(ret, bld.String())
				bld.Reset()
				continue
			}
		}

		// new-lines to standard whitespace
		// removes last new-line
		if v == '\n' {
			if len(s) == i+1 {
				break
			}

			if inQuote {
				bld.WriteRune(' ')
			}
			continue
		}
		bld.WriteRune(v)
	}

	ret = append(ret, bld.String())

	return ret
}
