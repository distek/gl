package cmd

import (
	"fmt"
	"strings"

	"github.com/spf13/cobra"
)

var restartCmd = &cobra.Command{
	Use:   "restart <service>[,<service>] | all",
	Short: "restart service(s)",
	ValidArgsFunction: func(
		cmd *cobra.Command,
		args []string,
		toComplete string,
	) ([]string, cobra.ShellCompDirective) {
		var ret []string

		ret = append(ret, strings.Split(client(compGetStarted, nil), "\n")[:]...)

		return ret, cobra.ShellCompDirectiveNoFileComp
	},
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Print(client("restart", args))
	},
}

func init() {
	rootCmd.AddCommand(restartCmd)
}
