package cmd

import (
	"fmt"
	"strings"

	"github.com/spf13/cobra"
)

var enableNowBool bool

var enableCmd = &cobra.Command{
	Use:   "enable <service>[,<service>]",
	Short: "enable service(s)",
	ValidArgsFunction: func(
		cmd *cobra.Command,
		args []string,
		toComplete string,
	) ([]string, cobra.ShellCompDirective) {
		var ret []string

		ret = append(ret, strings.Split(client(compGetAllDisabled, nil), "\n")[:]...)

		return ret, cobra.ShellCompDirectiveNoFileComp
	},
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Print(client(
			"enable",
			append([]string{fmt.Sprintf("%v", enableNowBool)}, args...),
		))
	},
}

func init() {
	enableCmd.Flags().BoolVar(&enableNowBool, "now", false,
		"start service as well as enable")

	rootCmd.AddCommand(enableCmd)
}
