package cmd

import (
	"fmt"
	"strings"

	"github.com/spf13/cobra"
)

var disableNowBool bool

var disableCmd = &cobra.Command{
	Use:   "disable <service>[,<service>]",
	Short: "disable service(s)",
	ValidArgsFunction: func(
		cmd *cobra.Command,
		args []string,
		toComplete string,
	) ([]string, cobra.ShellCompDirective) {
		var ret []string

		ret = append(ret, strings.Split(client(compGetAllEnabled, nil), "\n")[:]...)

		return ret, cobra.ShellCompDirectiveNoFileComp
	},
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Print(client(
			"disable",
			append([]string{fmt.Sprintf("%v", disableNowBool)}, args...),
		))
	},
}

func init() {
	disableCmd.Flags().BoolVar(&disableNowBool, "now", false,
		"stop service as well as disable")

	rootCmd.AddCommand(disableCmd)
}
